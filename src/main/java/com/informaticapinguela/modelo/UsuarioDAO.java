package com.informaticapinguela.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Named
@ApplicationScoped
public class UsuarioDAO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
    private DataSourceFactory dataSourceFactory ;
	
	public UsuarioDAO() {
		super();
	}
	
	
	public ArrayList<Usuario> getUsuarios() {
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT * FROM usuario");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Usuario usuario= new Usuario(rs.getString("nombre"), rs.getString("contrasena"), rs.getString("email"), rs.getString("direccion"), rs.getString("telefono"));
				usuarios.add(usuario);
			}
			conectUsers.close();
			ps.close();
			rs.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarios;
	}
	
	
	public void addUsuario(Usuario u){
		/// insert en al bd, deshaer el objeto otra vez para sacar los campos y hacer el insert en la tabla
	
		try {
				Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
				String sql= "INSERT INTO usuario (nombre, contrasena, email, direccion, telefono, rol) VALUES ('"+u.getNombre()+"','"+ u.getContrasena()+"','"+ u.getEmail()+"','"+ u.getDireccion()+"','"+ u.getTelefono()+"','"+Rol.ROLE_USER+"')";
				Statement s= conectUsers.createStatement();
				s.executeUpdate(sql);
				conectUsers.close();
				s.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


}
