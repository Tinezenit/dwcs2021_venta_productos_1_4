package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * Clase para obtener la información de los productos
 * @author Rubén Martínez Quiroga
 * @version 1.3
 */
@Named
@Dependent
public class Producto extends Categoria implements Serializable{
	//ATRIBUTOS
	private String nombre;
	private double precioUnidad;
	private int stock;
	private int id;
	private int comprarUnidades;

	//constructor con super el nombre de la categoria(clase padre) del producto
	/**
	 * Constructor de Producto
	 * @param id nuevo
	 * @param nombreCategoria nuevo
	 * @param nombreProducto nuevo
	 * @param precioUnidad nuevo
	 * @param stock nuevo
	 */
	public Producto(int id, String nombreCategoria, String nombreProducto, double precioUnidad, int stock) {
		super(nombreCategoria);
		this.id=id;
		this.nombre = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.stock = stock;
	}
	/**
	 * Constructor vacío
	 */
	public Producto() {
		super();
	}

	//GET Y SET
	/**
	 * Obtener el nombre del producto
	 * @return una string con el nombre del producto
	 */
	public String getNombreProducto() {
		return nombre;
	}
	/**
	 * Introducir un nombre de producto
	 * @param nombreProducto nuevo
	 */
	public void setNombreProducto(String nombreProducto) {
		this.nombre = nombreProducto;
	}
	/**
	 * Obtener el precio por unidad del producto
	 * @return un double con el precio del producto
	 */
	public double getPrecioUnidad() {
		return precioUnidad;
	}
	/**
	 * Introducir un precio para el producto
	 * @param precioUnidad nuevo
	 */
	public void setPrecioUnidad(double precioUnidad) {
		this.precioUnidad = precioUnidad;
	}
	/**
	 * Obtener su id
	 * @return un int con su id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Introducir un id
	 * @param id nuevo
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Obtener el stock
	 * @return un int con el número de stock
	 */
	public int getStock() {
		return stock;
	}
	/**
	 * Introducir un stock para el producto
	 * @param stock nuevo
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	/**
	 * Obtener las unidades compradas
	 * @return comprarUnidades 
	 */
	public int getComprarUnidades() {
		return comprarUnidades;
	}
	/**
	 * Introducir la cantidad de unidades que se van a comprar
	 * @param comprarUnidades nuevo
	 */
	public void setComprarUnidades(int comprarUnidades) {
		this.comprarUnidades = comprarUnidades;
	}
	/**
	 * Método para sacar la inforación de Producto
	 */
	@Override
	public String toString() {
		return "Producto [nombreProducto=" + nombre + ", precioUnidad=" + precioUnidad + ", stock=" + stock
				+ ", id=" + id + ", comprarUnidades=" + comprarUnidades + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
