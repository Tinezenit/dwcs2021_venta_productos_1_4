package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * Clase para obtener los roles
 * @author Rubén Martínez Quiroga
 * @version 1.3
 */
@Named
@Dependent
public enum Rol implements Serializable{
	ROLE_ADMIN,
	ROLE_USER;
}
