package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;

@Named
@ApplicationScoped
public class DataSourceFactory implements Serializable{

	private static final long serialVersionUID = 1L;
	private DataSource dataSource;

	public DataSourceFactory() throws ServletException {
	try {
               InitialContext ctx = new InitialContext();
               dataSource= (DataSource)ctx.lookup("java:jboss/datasources/MySqlDS");
		} catch (Exception e) {
		     throw new ServletException(e);
		     }
	}
	public DataSource getDataSource() {
		return dataSource;
	}
         public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}