package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
/**
 * Clase para obtener la información de los usuarios
 * @author Rubén Martínez Quiroga
 * @version 1.3
 */
@Named
@Dependent
public class Usuario implements Serializable{
	private static final long serialVersionUID= 1L;
	private String nombre;
	private String contrasena;
	private String email;
	private String direccion;
	private String telefono;
	private int id;
	private Rol rol;

	/**
	 * Cosntante para el rol usuario
	 */
	public static final String ROLE_USER="ROLE_USER"; 
	/**
	 * Constante para el rol admin
	 */
	public static final String ROLE_ADMIN="ROLE_ADMIN"; 

	/**
	 * Constructor de la clase
	 * @param nombre nuevo
	 * @param contrasena nueva
	 * @param email nuevo
	 * @param direccion nueva
	 * @param telefono nuevo
	 * @param rol nuevo
	 */
	public Usuario(String nombre, String contrasena, String email, String direccion, String telefono, Rol rol) {
		super();
		this.nombre = nombre;
		this.contrasena = contrasena;
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
		this.rol=rol;
	}
	/**
	 * Introducir un rol al usuario
	 * @param rol nuevo
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	/**
	 * Constructor de usuario vacío
	 */
	public Usuario() {
		super();
	}
	/**
	 * Constructor con email para la constante USUARIO_VACIO
	 * @param email nuevo
	 */
	public Usuario(String email) {
		super();
		this.email=email;
	}
	/**
	 * Constructor de la clase
	 * @param nombre nuevo
	 * @param contrasena nueva
	 * @param email nuevo
	 * @param direccion nueva
	 * @param telefono nuevo
	 */
	public Usuario(String nombre, String contrasena, String email, String direccion, String telefono) {
		super();
		this.nombre = nombre;
		this.contrasena = contrasena;
		this.email = email;
		this.direccion = direccion;
		this.telefono = telefono;
	}
	/**
	 * Obtener el rol del usuario
	 * @return el rol del usuario
	 */
	public Rol getRol() {
		return rol;
	}
	/**
	 * Obtener el id del usuario
	 * @return un int con el id del usuario
	 */
	public int getId() {
		return id;
	}
	/**
	 * Introduce una id para el usuario
	 * @param id nueva
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Devuelve el nombre guardado
	 * @return nombre guardado
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Configura un nuevo nombre
	 * @param nombre nuevo
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Devuelve la contraseña guardada
	 * @return contrasena guardada
	 */
	public String getContrasena() {
		return contrasena;
	}
	/**
	 * Configura una nueva contraseña
	 * @param contrasena nueva
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	/**
	 * Devuelve el email guardado
	 * @return email guardado
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Configura un nuevo email
	 * @param email nuevo
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Devuelve la dirección guardada
	 * @return dirección guardada
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * Configura una nueva dirección
	 * @param direccion nueva
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * Devuelve el teléfono guardado
	 * @return teléfono guardado
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * Configura un nuevo teléfono
	 * @param telefono nuevo
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * Devuelve los datos de la clase
	 */
	@Override
	public String toString() {
		return nombre + "--" + email + "--" + direccion + "--" + telefono + "--" + rol+"\n";  //salto de linea para mostrar la lista correctamente(necesita arreglos)
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
}
