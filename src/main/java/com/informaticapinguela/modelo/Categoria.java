package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

/**
 * Clase para obtener la información de las categorías
 * @author Rubén Martínez Quiroga
 * @version 1.3
 */

@Named
public class Categoria implements Serializable{
	private static final long serialVersionUID = 1L;
	//ATRIBUTOS
	protected  String nombreCategoria;
		
	//CONSTRUCTOR CON SUPER PARA HERENCIA
	/**
	 * Constructor de la clase
	 * @param nombreCategoria nuevo
	 */
	public Categoria(String nombreCategoria) {
		super();
		this.nombreCategoria=nombreCategoria;
	}
	/**
	 * Constructor vacío
	 */
	public Categoria() {
			
	}

	//GET Y SET
	/**
	 * Devuelve el nombre guardado
	 * @return nombre guardado
	 */
	public String getNombreCategoria() {
		return nombreCategoria;
	}
	/**
	 * Configura un nuevo nombre
	 * @param nombreCategoria nuevo
	 */
	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
		
	//toString para sacar los datos
	/**
	 * Devuelve los datos de la clase
	 */
	public String toString() {
		return nombreCategoria;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombreCategoria == null) ? 0 : nombreCategoria.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		if (nombreCategoria == null) {
			if (other.nombreCategoria != null)
				return false;
		} else if (!nombreCategoria.equals(other.nombreCategoria))
			return false;
		return true;
	}
	
}
