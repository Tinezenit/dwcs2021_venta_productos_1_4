package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;


/**
 * Clase modelo para obtener la información de los productos
 * @author Rubén Martínez Quiroga
 * @version 1.3
 */
@Named
@ApplicationScoped
public class ProductoDAO implements Serializable{
	ArrayList<Producto> productos= new ArrayList<Producto>();
	ArrayList<Categoria> categorias= new ArrayList<Categoria>();
	Producto producto;
	
	@Inject
    private DataSourceFactory dataSourceFactory ;

	/**
	 * Método para obtener todos los productos
	 * @return arrayList de productos
	 */
	public ArrayList<Producto> getProductos() {
		productos = new ArrayList<Producto>();
		try {
			Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT * FROM producto");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Producto producto= new Producto(rs.getInt("id"), rs.getString("nombre_categoria"), rs.getString("nombre"), rs.getDouble("precio"), rs.getInt("stock"));
					productos.add(producto);
			}
			conectUsers.close();
			rs.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productos;
	}
	
	public ArrayList<Categoria> getCategoria() {
		categorias= new ArrayList<Categoria>();
		try {
			Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT nombre FROM categoria");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Categoria categoria= new Categoria(rs.getString("nombre"));
					categorias.add(categoria);
			}
			conectUsers.close();
			rs.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categorias;
	}
	
	
	
	
	/**
	 * Método para añadir un producto
	 * @param p nuevo
	 * @throws SQLException nueva
	 */
	public void addProducto(Producto p){
		/// insert en al bd, deshaer el objeto otra vez para sacar los campos y hacer el insert en la tabla
		try {
			Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			String sql= "INSERT INTO producto (nombre_categoria, nombre, precio, stock) VALUES ('"+p.getNombreCategoria()+"','"+p.getNombreProducto()+"','"+ p.getPrecioUnidad()+"','"+ p.getStock()+"')";
			Statement s= conectUsers.createStatement();
			s.executeUpdate(sql);
			conectUsers.close();
			s.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Método para actualizar un producto ya existente
	 * @param id nuevo
	 * @param nombreCategoria nuevo
	 * @param nombreProducto nuevo
	 * @param precioUnidad nuevo
	 * @param stock nuevo
	 * @throws SQLException nueva
	 */
	public void actualizarProducto(Producto p){ 
		try {
			Connection c= dataSourceFactory.getDataSource().getConnection();
			String sql="UPDATE producto SET nombre_Categoria='"+p.getNombreCategoria()+"', nombre='"+p.getNombreProducto()+"', precio='"+p.getPrecioUnidad()+"', stock='"+p.getStock()+"' WHERE id='"+p.getId()+"'";  //cambiar por p.nombreProducto etc etc
		//	PreparedStatement s = c.prepareStatement("UPDATE producto SET nombre_Categoria=?, nombre=?, precio=?, stock=? WHERE id=?");
			Statement s= c.createStatement();
		//	s.setString(1, p.getNombreCategoria());
		//	s.setString(2, p.getNombreProducto());
		//	s.setDouble(3, p.getPrecioUnidad());
		//	s.setInt(4, p.getStock());
		//	s.setInt(5, p.getId());
			s.executeUpdate(sql);
			//s.executeUpdate();
			c.close();
			s.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void actualizarProducto(ArrayList<Producto> productos) {
		for (Producto p: productos) {
			actualizarProducto(p);
		}
	}

	public void borrar(Producto p) {
		try {
			Connection c= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement s = c.prepareStatement("DELETE FROM producto WHERE id = ?");
			s.setInt(1,p.getId());
			s.executeUpdate();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//return "listarProductos.xhtml?faces-redirect=true";
	}
	
	/**
	 * Método que busca un producto según su id
	 * @param id nuevo
	 * @return un producto
	 */
	 public Producto buscarId(int id) {
		     producto = null;
		    for (Producto producto : productos) { //recorro los productos que hay en lsita
		        if (producto.getId()==(id)) {   //obtengo la id del producto y si es igual a la id introducida entonces devuelve ese producto
		        	this.producto = producto;
		            break;
		        }
		    }
		    return producto;
		}
	 
	 /**
	  * Muestra los productos pertenecientes a la categoría videojuegos
	  * @return arrayList de productos
	  * @throws SQLException nueva
	  */
	 public ArrayList<Producto> mostrarVideojuegos() throws SQLException{
		 try {
		 productos = new ArrayList<Producto>();
		 Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT * FROM producto WHERE nombre_categoria='videojuego'");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Producto productoV= new Producto(rs.getInt("id"), rs.getString("nombre_categoria"), rs.getString("nombre"), rs.getDouble("precio"), rs.getInt("stock"));
				productos.add(productoV);
			}
			conectUsers.close();
			rs.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return productos;
	 }
	 
	 /**
	  * Muestra los productos pertenecientes a la categoría películas
	  * @return arrayList de productos
	  * @throws SQLException nueva
	  */
	 public ArrayList<Producto> mostrarPeliculas() throws SQLException{
		 try {
		 productos = new ArrayList<Producto>();
		 Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT * FROM producto WHERE nombre_categoria='pelicula'");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Producto productoP= new Producto(rs.getInt("id"), rs.getString("nombre_categoria"), rs.getString("nombre"), rs.getDouble("precio"), rs.getInt("stock"));
				productos.add(productoP);
			}
			conectUsers.close();
			ps.close();
			rs.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return productos;
	 }
	 
	 /**
	  * Muestra los productos pertenecientes a la categoría software
	  * @return arrayList de productos
	  * @throws SQLException nueva
	  */
	 public ArrayList<Producto> mostrarSoftware() throws SQLException{
		 try {
		 productos = new ArrayList<Producto>();
		 Connection conectUsers= dataSourceFactory.getDataSource().getConnection();
			PreparedStatement ps = conectUsers.prepareStatement("SELECT * FROM producto WHERE nombre_categoria='software'");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Producto productoS= new Producto(rs.getInt("id"), rs.getString("nombre_categoria"), rs.getString("nombre"), rs.getDouble("precio"), rs.getInt("stock"));
				productos.add(productoS);
			}
			conectUsers.close();
			rs.close();
			}catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return productos;
	 }
	 

	 
	 
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((productos == null) ? 0 : productos.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ProductoDAO other = (ProductoDAO) obj;
			if (productos == null) {
				if (other.productos != null)
					return false;
			} else if (!productos.equals(other.productos))
				return false;
			return true;
		}
	
}
