package com.informaticapinguela.backing;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.informaticapinguela.modelo.Categoria;
import com.informaticapinguela.modelo.Producto;
import com.informaticapinguela.modelo.ProductoDAO;

@Named
@SessionScoped
public class ProductoBacking implements Serializable{

	private static final long serialVersionUID = 1L;
	ArrayList<Producto> productos= new ArrayList<Producto>();
	ArrayList<Categoria> categorias= new ArrayList<Categoria>();
	
	private boolean nuevo=false;
	private boolean editar=false;

	
	@Inject
	private Producto productoEditar;
	
	@Inject
	private ProductoDAO productoDAO;
	
	@Inject
	private Producto producto;
	
	@Inject
	private Categoria categoria;
	
	
	public ProductoBacking() {
		super();
	}

	@PostConstruct
	public void init() {
		this.productos=productoDAO.getProductos();
		this.categorias=productoDAO.getCategoria();
	}
	
	
	public ArrayList<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(ArrayList<Categoria> categorias) {
		this.categorias = categorias;
	}
	
	
	public void delete (Producto p) {
		productoDAO.borrar(p);
		productos.remove(p);
		//return "listarProductos.xhtml?faces-redirect=true";
	}
	
	public void nuevoProducto () {
		productoDAO.addProducto(producto);
		this.producto=new Producto();
		this.productos=productoDAO.getProductos();
		nuevo=false;
	}
	
	public void editar(Producto p) {
		editar=true;
		productoEditar=p;
	}
	
	public void actualizar() {
		productoDAO.actualizarProducto(productoEditar);
		this.productos=productoDAO.getProductos();
		this.productoEditar= new Producto();
		editar=false;
	}
	
	
	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	
	public boolean isNuevo() {
		return nuevo;
	}
	
	public boolean nuevoV() {
		nuevo=true;
		return nuevo;
	}

	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	public ProductoDAO getProductoDAO() {
		return productoDAO;
	}

	public void setProductoDAO(ProductoDAO productoDAO) {
		this.productoDAO = productoDAO;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isEditar() {
		return editar;
	}
	
	public void setEditar(boolean editar) {
		this.editar = editar;
	}
}
